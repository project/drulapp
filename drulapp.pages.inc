<?php

/**
 * @file
 * User page callbacks for the drulapp module.
 */

/**
 * Menu callback; Process an drulapp authentication.
 */
function drulapp_domain_key_page() {
  if (!empty($_POST) && !empty($_POST['appversion'])) {
    $info = system_get_info('module', 'drulapp');
    $dkey = variable_get('drulapp_dkey', '');
    // Last time at which domain key was generated.
    $dkey_timestamp = variable_get('drulapp_dkey_timestamp', '');
    $data['dkey_timestamp'] = $dkey_timestamp;
    $code_length = variable_get('drulapp_code_length', DRULAPP_CODELENGTH);
    $allowed_validity = variable_get('drulapp_auth_validity', DRULAPP_AUTH_VALIDITY);
    $data['version'] = $info['version'];
    $data['dkey'] = $dkey;
    // Allowed characters in phone auth code.
    $data['allowed_chars'] = DRULAPP_NUMBERS;
    $data['code_length'] = $code_length;
    $data['validity'] = $allowed_validity;
    return drupal_json_output($data);
  }
  // Only post allowed.
  $data['error_code'] = t('101');
  return drupal_json_output($data);
}

/**
 * Menu callback; Send user info.
 */
function drulapp_user_key_page() {
  if (!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['devicekey'])) {
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $devicekey = trim($_POST['devicekey']);

    $data = drulapp_valid_user($username, $password);
    if (!isset($data['error_code']) && (NULL !== $uid = $data['uid'])) {
      $domainkey = variable_get('drulapp_dkey', '');
      $data = drulapp_user_authkey($uid, $username, $domainkey, $devicekey);
      return drupal_json_output($data);
    }
    return drupal_json_output($data);
  }
  else {
    // Only post allowed.
    $data['error_code'] = t("101");
    return drupal_json_output($data);
  }
}

/**
 * Menu callback; to get user auth api for the specified user.
 */
function drulapp_valid_user($username, $password) {
  // Store email for auth key generation.
  $user_email = $username;
  if (!flood_is_allowed('failed_drulapp_user_authkey_ip', variable_get('user_failed_login_ip_limit', 50), variable_get('user_failed_login_ip_window', 3600)) || !flood_is_allowed('failed_drulapp_user_authkey_ip', variable_get('user_failed_login_ip_limit', 10), variable_get('user_failed_login_ip_window', 3600), $user_email)) {
    // User blocked for 15 mins.
    $output['error_code'] = '102';
    return $output;
  }

  if (valid_email_address($username)) {
    if ($account = user_load_by_mail($username)) {
      $username = $account->name;
    }
    else {
      // Account not found for that email.
      // Always register an IP-based failed login event.
      flood_register_event('failed_drulapp_user_authkey_ip', variable_get('user_failed_login_ip_window', 3600));
      // Account not found for email id or blocked.
      $output['error_code'] = '103';
      return $output;
    }
  }

  if (user_is_blocked($username)) {
    // Always register an IP-based failed login event.
    flood_register_event('failed_drulapp_user_authkey_ip', variable_get('user_failed_login_ip_window', 3600));
    // Account found but user blocked.
    $output['error_code'] = '104';
    return $output;
  }
  elseif (($uid = user_authenticate($username, $password)) != FALSE) {
    $output['uid'] = $uid;
    flood_clear_event('failed_drulapp_user_authkey_ip', $user_email);
    return $output;
  }
  else {
    // Always register an IP-based failed login event
    // block ip for further attempt based on email id.
    flood_register_event('failed_drulapp_user_authkey_ip', variable_get('user_failed_login_ip_window', 3600), $user_email);
    // Email / password authentication failed.
    $output['error_code'] = '105';
    return $output;
  }
}

/**
 * Helper function to create/update user auth key.
 */
function drulapp_user_authkey($uid, $user_email, $domainkey, $devicekey) {
  $authuser[$uid] = db_select('drulapp', 'd')
    ->fields('d', array('dlaid', 'halfkey', 'authkey', 'bchars'))
    ->condition('uid', $uid)
    ->execute()
    ->fetchObject();

  $newkey = drulapp_authkey_crypt($user_email, $domainkey, $devicekey);
  $buttons = (variable_get('drulapp_custom_chars', FALSE)) ? drulapp_buttonchars() : DRULAPP_BUTTONS;

  // User already have auth key . Forcefully update if dlaid key is set.
  if (isset($authuser[$uid]->dlaid)) {
    $buttons = (variable_get('drulapp_custom_chars', FALSE)) ? drulapp_buttonchars($authuser[$uid]->bchars) : DRULAPP_BUTTONS;
    $dlaid = $authuser[$uid]->dlaid;
    // Update existing user record.This happen when user unintall
    // and install the app and perform login.
    db_update('drulapp')
      ->fields(array(
        'uid' => $uid,
        'bchars' => $buttons,
        'authkey' => $newkey['authkey'],
        'halfkey' => $newkey['halfkey_temp'],
        'devicekey' => $devicekey,
        'changed' => $newkey['last_auth'],
      )
      )
      ->condition('dlaid', $dlaid)
      ->execute();
  }
  else {
    // Insert the record with generated authkey.
    $dlaid = db_insert('drulapp')
      ->fields(array(
        'uid' => $uid,
        'bchars' => $buttons,
        'authkey' => $newkey['authkey'],
        'halfkey' => $newkey['halfkey_temp'],
        'devicekey' => $devicekey,
        'created' => REQUEST_TIME,
        'changed' => $newkey['last_auth'],
      )
      )
      ->execute();
  }

  unset($newkey['halfkey_temp']);
  $newkey['dlaid'] = $dlaid;
  return $newkey;
}

/**
 * Helper function to generate user auth key.
 */
function drulapp_authkey_crypt($user_email, $domainkey, $devicekey) {
  $output = '';
  $randomkey1 = rand(7, 13);
  $randomkey2 = rand(3, 7);
  $count = $randomkey1 + $randomkey2;
  $algo = 'sha512';

  $hash = hash($algo, $user_email . $domainkey . $devicekey, TRUE);
  do {
    if ($count == $randomkey2) {
      $len = strlen($hash);
      $hash = _password_base64_encode($hash, $len);
      $halfkey2 = substr($hash, floor(strlen($hash) / 2));
      $halfkey1 = '$' . $randomkey2 . '$' . substr($hash, 0, floor(strlen($hash) / 2));
    }
    $hash = hash($algo, $hash . $devicekey, TRUE);
  } while (--$count);

  $len = strlen($hash);
  $userkey = _password_base64_encode($hash, $len);

  $output['halfkey'] = $halfkey1;
  $output['authkey'] = $userkey;
  $output['halfkey_temp'] = $halfkey2;
  $output['last_auth'] = REQUEST_TIME;
  return $output;
}

/**
 * Helper function to generate user auth key.
 */
function drulapp_active_configuration() {
  if (!empty($_POST) && !empty($_POST['halfkey']) && !empty($_POST['dlaid']) && !empty($_POST['devicekey'])) {
    $halfkey = trim($_POST['halfkey']);
    $devicekey = trim($_POST['devicekey']);
    $dlaid = trim($_POST['dlaid']);
    if ($halfkey[0] != '$' || $halfkey[2] != '$') {
      // Invalid half key.
      $output['error_code'] = '106';
      return $output;
    }
    $data = drulapp_getactive_configuration($halfkey, $devicekey, $dlaid);
    return drupal_json_output($data);
  }
  // Only post allowed.
  $data['error_code'] = t("101");
  return drupal_json_output($data);
}

/**
 * Helper function to get active configuration of logged in user.
 */
function drulapp_getactive_configuration($halfkey, $devicekey, $dlaid) {
  $algo = 'sha512';

  require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
  $authuser[$dlaid] = db_select('drulapp', 'd')
    ->fields('d', array('halfkey', 'uid'))
    ->condition('dlaid', $dlaid)
    ->condition('devicekey', $devicekey)
    ->execute()
    ->fetchObject();
  if (empty($authuser[$dlaid])) {
    // Invalid device id or drulapp id.
    $output['error_code'] = '107';
    return $output;
  }

  if (!isset($authuser[$dlaid]->halfkey)) {
    // Missing device id or user not authenticated yet.
    $output['error_code'] = '108';
    return $output;
  }

  $halfkey1 = substr($halfkey, 3);
  $halfkey2 = $authuser[$dlaid]->halfkey;
  $count = substr($halfkey, 1, 1);
  $hash = $halfkey1 . $halfkey2;

  for ($i = 0; $i < $count; $i++) {
    $hash = hash($algo, $hash . $devicekey, TRUE);
    $len = strlen($hash);
  }
  $len = strlen($hash);
  $userkey = _password_base64_encode($hash, $len);

  $authuser[$dlaid] = db_select('drulapp', 'd')
    ->fields('d', array('bchars'))
    ->condition('authkey', $userkey)
    ->condition('devicekey', $devicekey)
    ->execute()
    ->fetchObject();

  if (empty($authuser[$dlaid])) {
    // Drulapp user auth key not found.
    $output['error_code'] = '109';
  }
  else {
    // User authenicated with device key and halfkey.
    $buttons = (variable_get('drulapp_custom_chars', FALSE)) ? drulapp_buttonchars($authuser[$dlaid]->bchars) : DRULAPP_BUTTONS;
    $last_auth = REQUEST_TIME;

    db_update('drulapp')
      ->fields(array(
        'bchars' => $buttons,
        'changed' => $last_auth,
      )
      )
      ->condition('authkey', $userkey)
      ->condition('devicekey', $devicekey)
      ->condition('dlaid', $dlaid)
      ->execute();
    $output['bchars'] = $buttons;
    $output['last_auth'] = $last_auth;
  }
  return $output;
}

/**
 * Helper function to check user is blocked.
 */
function drulapp_userid_is_blocked($uid) {
  return db_select('users')
    ->fields('users', array('name'))
    ->condition('uid', $uid)
    ->condition('status', 0)
    ->execute()->fetchObject();
}
