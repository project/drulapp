<?php

/**
 * @file
 * Admin page callbacks for the drulapp module.
 */

/**
 * Implements hook_permission().
 */
function drulapp_permission() {
  $permissions = array();
  $permissions['administer drulapp'] = array(
    'title' => t('Administer drulapp'),
    'description' => t('Administer drulapp configuration settings.'),
  );
  return $permissions;
}

/**
 * Display drulapp settings form (borrowed from path2ban).
 */
function drulapp_settings() {
  // @TODO: periodic domain key change.
  $form = array();
  $form['drulapp_options'] = array(
    '#type' => 'fieldset',
  );
  $form['drulapp_options']['drulapp_dkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Active Domain key'),
    '#description' => t('Domain key generated on first install. This will be cleared only on module uninstall.'),
    '#disabled' => TRUE,
    '#default_value' => variable_get('drulapp_dkey', ''),
  );
  $form['drulapp_options']['drulapp_dkey_timestamp'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain key generated at'),
    '#description' => t('Timestamp on which Domain key is generated.This will force user to re-login from app if Domain key is regenerated.'),
    '#disabled' => TRUE,
    '#default_value' => variable_get('drulapp_dkey_timestamp', REQUEST_TIME),
  );
  // Not make sense to show anonymous, as we are showing phonecode
  // screen after user/ password authentication only.
  $system_roles = user_roles();
  unset($system_roles[DRUPAL_ANONYMOUS_RID]);

  $form['drulapp_options']['drulapp_applicable_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('drulapp_applicable_roles', array()),
    '#options' => $system_roles,
    '#description' => t('Show phone code authentication to these roles.If none are selected, all users must pass through phone code authentication.'),
  );

  $allowed_validity = variable_get('drulapp_auth_validity', DRULAPP_AUTH_VALIDITY);
  $allowed_validity_options = array(
    DRULAPP_AUTH_VALIDITY => t('1 Day'),
    (DRULAPP_AUTH_VALIDITY * 7) => t('7 Days)'),
    (DRULAPP_AUTH_VALIDITY * 15) => t('15 Days'),
    (DRULAPP_AUTH_VALIDITY * 30) => t('30 Days'),
  );
  $form['drulapp_options']['drulapp_auth_validity'] = array(
    '#type' => 'select',
    '#title' => t('Authentication validation period'),
    '#options' => $allowed_validity_options,
    '#default_value' => $allowed_validity,
    '#description' => t('User must reauthenticate atleast once from <a href="@app-link" target="_new">Drulapp</a> after this period expired.', array('@app-link' => 'https://play.google.com/store/apps/details?id=com.drulapp.drulapp&hl=en')),
  );

  $code_length = variable_get('drulapp_code_length', 6);
  $code_length_options = array(
    4 => t('4'),
    6 => t('6'),
    8 => t('8'),
  );
  $form['drulapp_options']['drulapp_code_length'] = array(
    '#type' => 'select',
    '#title' => t('character length of generating code'),
    '#options' => $code_length_options,
    '#default_value' => $code_length,
    '#description' => t("total number of characters that should be generated"),
  );
  $form['drulapp_options']['drulapp_custom_chars'] = array(
    '#type' => 'checkbox',
    '#title' => t("Pick Random characters from A-Z."),
    '#description' => t('Shows randomly selected chars for button shown on the phone. If disabled set to default A-B-C-D. Note: Enabling this will make each user have different button characters which is good for security.'),
    '#default_value' => variable_get('drulapp_custom_chars', TRUE),
    '#attributes' => array('checked' => 'checked'),
  );

  return system_settings_form($form);
}

/**
 * Validation for the administrative settings form(borrowed from smtp).
 */
function drulapp_settings_validate($form, &$form_state) {

}
